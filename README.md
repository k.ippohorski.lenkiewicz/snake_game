# Tile Snake Game

Tile Snake is a Python-based game developed using the Pygame library. It offers a classic snake gameplay experience with a twist of tile-based graphics and engaging sound effects. Navigate your snake through the dark grass, eat food to grow longer, and avoid colliding with the walls or yourself!

## Features

- **Tile-Based Graphics**: Enjoy a retro, tile-based aesthetic reminiscent of classic arcade games.
- **Dynamic Snake Movement**: Control the snake with arrow keys or WASD, offering smooth and responsive gameplay.
- **Custom Sound Effects**: Each action in the game, from eating food to the inevitable collision, comes with its unique sound effect for an immersive experience.
- **High Score Tracking**: Keep pushing your limits with an in-game high score tracker.

## Getting Started

### Prerequisites

Ensure you have Python installed on your system. Tile Snake requires Python 3.x and Pygame. You can install Pygame using pip:

```bash
pip install pygame
```

### Installation

1. Clone the repository or download the source code.
2. Extract the contents if downloaded as a zip file.
3. Navigate to the game's directory in your terminal or command prompt.
4. Run the game with Python:

```bash
python main.py
```

### Controls

- **Arrow Keys**: Move Up, Down, Left, Right
- **WASD**: Alternative movement controls
- **Spacebar**: Restart the game after a game over
- **Escape**: Quit the game at any time

## Development

Tile Snake was developed with a focus on simplicity and performance. The game utilizes the Pygame library for rendering, sound effects, and handling user input.

- **Python**: The core programming language used.
- **Pygame**: A Python library for creating video games.

### Customizing

Feel free to dive into the code to customize aspects like the snake's speed, the game's difficulty, or even adding new features like power-ups or different levels.

## Credits

- **Graphics**: 2D art by Hyptosis and Zabin from opengameart.org.
- **Music**: Background tracks by Umplix from opengameart.org.
- **Sound Effects**: Inspired by classic games, including MGS 1.

## Support

If you encounter any issues or have suggestions for future updates, please feel free to open an issue on the repository or submit a pull request with your improvements.

## License

This project is open-source and available under the MIT License. See the LICENSE file for more details.
