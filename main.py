import pygame
import random
from sys import exit


class Snake:
    def __init__(self, arena_grid_cells, cell_size):
        # assets
        self.image = snake_tile
        self.skull = snake_tile_skull
        self.grow_sound = pygame.mixer.Sound('assets/audio/codecopen.wav')
        self.death_sound = pygame.mixer.Sound('assets/audio/codecover.wav')
        
        # setting up arena bounds
        self.arena_grid = arena_grid_cells
        self.cell_size = cell_size
        self.x_of_death = [self.arena_grid['columns'][0] * self.cell_size - self.cell_size,
                           self.arena_grid['columns'][-1] * self.cell_size + self.cell_size]
        self.y_of_death = [self.arena_grid['rows'][0] * self.cell_size - self.cell_size,
                           self.arena_grid['rows'][-1] * self.cell_size + self.cell_size]
        
        # initializing snake on random position
        x = random.choice(self.arena_grid['columns']) * self.cell_size
        y = random.choice(self.arena_grid['rows']) * self.cell_size
        self.positions = [(x, y)]
        
        # setup finish
        self.length = 1
        self.moving_direction = None
        self.grow_snake = False

    def get_input(self):
        input_key = pygame.key.get_pressed()
        
        if (input_key[pygame.K_w] or input_key[pygame.K_UP]) and self.moving_direction != 'down':
            self.moving_direction = 'up'
        elif (input_key[pygame.K_s] or input_key[pygame.K_DOWN]) and self.moving_direction != 'up':
            self.moving_direction = 'down'
        elif (input_key[pygame.K_a] or input_key[pygame.K_LEFT]) and self.moving_direction != 'right':
            self.moving_direction = 'left'
        elif (input_key[pygame.K_d] or input_key[pygame.K_RIGHT]) and self.moving_direction != 'left':
            self.moving_direction = 'right'

    def move(self):
        if self.moving_direction is None:
            return False
        
        head_x, head_y = self.get_head_position()
        if self.moving_direction == 'up':
            head_y -= self.cell_size
        elif self.moving_direction == 'down':
            head_y += self.cell_size
        elif self.moving_direction == 'left':
            head_x -= self.cell_size
        elif self.moving_direction == 'right':
            head_x += self.cell_size
        
        new_head_position = (head_x, head_y)
        self.positions.insert(0, new_head_position)
        
        if not self.grow_snake:
            self.positions.pop()
        self.grow_snake = False
        
        return new_head_position

    def draw(self, surface, is_dead):
        if is_dead:
            image_asset = self.skull
        else:
            image_asset = self.image
        
        for position in self.positions:
            surface.blit(image_asset, position)

    def get_head_position(self):
        return self.positions[0]

    def eat_food(self, food_position):
        if self.get_head_position() == food_position:
            self.grow_snake = True
            self.grow_sound.play()
            return True
        return False

    def death_check(self):
        head_position = self.get_head_position()
        if (head_position[0] in self.x_of_death
                or head_position[1] in self.y_of_death
                or self.positions.count(head_position) > 1):
            self.death_sound.play()
            return True
        return False


class Food:
    def __init__(self, arena_grid_cells, cell_size, surface):
        self.image = pygame.image.load('assets/graphics/shroom.png').convert_alpha()
        self.arena_grid = arena_grid_cells
        self.cell_size = cell_size
        self.screen_surface = surface
        self.position = self.reposition([])

    def reposition(self, snake_positions):
        while True:
            x = random.choice(self.arena_grid['columns']) * self.cell_size
            y = random.choice(self.arena_grid['rows']) * self.cell_size
            if (x, y) not in snake_positions:
                self.position = (x, y)
                return self.position

    def draw(self):
        self.screen_surface.blit(self.image, self.position)
        
        
def draw_multiline_text(surface, font, text_list, first_line_y_pos):
    line_y_pos = first_line_y_pos
    
    for line in text_list:
        rendered_line = font.render(f'  {line}  ', False, text_fg_color)
        line_rect = rendered_line.get_rect(center=(screen_width / 2, line_y_pos))
        pygame.draw.rect(surface, text_bg_color, line_rect)
        surface.blit(rendered_line, line_rect)
        line_y_pos += 25


def draw_intro_screen(surface, font):
    intro_tiles = [snake_tile, snake_tile_skull]
    for row in range(GRID_HEIGHT):
        if row % 2 == 0:
            tile_index_to_blit = 0
        else:
            tile_index_to_blit = 1
            
        for column in range(GRID_WIDTH):
            surface.blit(
                intro_tiles[tile_index_to_blit],
                (column * CELL_SIZE, row * CELL_SIZE)
            )
            if tile_index_to_blit == 0:
                tile_index_to_blit += 1
            else:
                tile_index_to_blit = 0
    
    header_y_pos = 35
    header_text = [
        '...TILE SNAKE...',
        'a game by jachulczyk',
        'use arrows or WASD to move',
        '!!! keep away from the dark grass !!!',
        'press space to start'
    ]
    draw_multiline_text(surface, font, header_text, header_y_pos)
    
    footer_y_pos = 255
    footer_text = [
        '2D art by Hyptosis and Zabin',
        'opengameart.org/users/hyptosis',
        'opengameart.org/users/zabin',
        'Music by Umplix',
        'opengameart.org/users/umplix',
        'Sound FX from MGS 1'
    ]
    draw_multiline_text(surface, font, footer_text, footer_y_pos)


def get_tile_type(row, column, grid_width, grid_height):
    top_gutter = grid_height - SCORE_CONTAINER_HEIGHT+1
    bottom_gutter = grid_height - SCORE_CONTAINER_HEIGHT
    
    # corners
    if (row == 0 or row == top_gutter) and column == 0:
        return "top-left"
    elif (row == 0 or row == top_gutter) and column == grid_width-1:
        return "top-right"
    elif (row == grid_height-1 or row == bottom_gutter) and column == 0:
        return "bottom-left"
    elif (row == grid_height-1 or row == bottom_gutter) and column == grid_width-1:
        return "bottom-right"
    
    # edges
    elif row == 0 or row == top_gutter:
        return "top-middle"
    elif row == grid_height - 1 or row == bottom_gutter:
        return "bottom-middle"
    elif column == 0:
        return "middle-left"
    elif column == grid_width - 1:
        return "middle-right"
    
    # center
    else:
        return "center"


def bake_background_surface(grid_width, grid_height, cell_size):
    background_surfaces = {
        "top-left": pygame.image.load('assets/graphics/bg-tile-tl.png').convert(),
        "top-middle": pygame.image.load('assets/graphics/bg-tile-t.png').convert(),
        "top-right": pygame.image.load('assets/graphics/bg-tile-tr.png').convert(),
        "middle-left": pygame.image.load('assets/graphics/bg-tile-l.png').convert(),
        "center": pygame.image.load('assets/graphics/bg-tile-c.png').convert(),
        "middle-right": pygame.image.load('assets/graphics/bg-tile-r.png').convert(),
        "bottom-left": pygame.image.load('assets/graphics/bg-tile-bl.png').convert(),
        "bottom-middle": pygame.image.load('assets/graphics/bg-tile-b.png').convert(),
        "bottom-right": pygame.image.load('assets/graphics/bg-tile-br.png').convert()
    }
    
    bg_surface = pygame.Surface((grid_width * cell_size, grid_height * cell_size))
    for row in range(grid_height):
        for column in range(grid_width):
            tile_type = get_tile_type(row, column, grid_width, grid_height)
            bg_surface.blit(
                background_surfaces[tile_type],
                (column * cell_size, row * cell_size)
            )
    
    return bg_surface


def draw_score(surface, score, font):
    score_surface = font.render(f'Score: {score}', False, text_fg_color)
    score_rect = score_surface.get_rect(center=(screen_width / 2, screen_height - 1.5 * CELL_SIZE))
    surface.blit(score_surface, score_rect)


def draw_game_over(surface, score, font):
    game_over_surface = font.render(f'Game Over! Score: {score} (restart: space)', False, text_fg_color)
    game_over_rect = game_over_surface.get_rect(center=(screen_width / 2, screen_height - 1.5 * CELL_SIZE))
    surface.blit(game_over_surface, game_over_rect)


pygame.init()

GRID_WIDTH, GRID_HEIGHT = 16, 13
CELL_SIZE = 32
SCORE_CONTAINER_HEIGHT = 4
screen_width = CELL_SIZE * GRID_WIDTH
screen_height = CELL_SIZE * GRID_HEIGHT
screen = pygame.display.set_mode((screen_width, screen_height))
pygame.display.set_caption('Tile Snake')

arena_grid = {
    'rows': list(range(1, GRID_HEIGHT - SCORE_CONTAINER_HEIGHT)),
    'columns': list(range(1, GRID_WIDTH - 1))
}

clock = pygame.time.Clock()
snake_move_interval = 200
start_time = 0
score = 0

font = pygame.font.Font('assets/font/Pixeltype.ttf', 40)
text_fg_color = (128, 126, 111)
text_bg_color = (255, 252, 227)
music = pygame.mixer.Sound('assets/audio/level_1_8-bit_.ogg')
music.set_volume(0.4)
music.play(loops=-1)

background_surface = bake_background_surface(GRID_WIDTH, GRID_HEIGHT, CELL_SIZE)
# snake surfaces declared outside of class for use in intro screen
snake_tile = pygame.image.load('assets/graphics/snake-tile-b1.png').convert()
snake_tile_skull = pygame.image.load('assets/graphics/snake-tile-skull.png').convert()

snake = Snake(arena_grid, CELL_SIZE)
food = Food(arena_grid, CELL_SIZE, screen)

last_move_time = pygame.time.get_ticks()
display_intro = True
game_active = True
while True:
    if display_intro:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    display_intro = False
                    
        draw_intro_screen(screen, font)
        pygame.display.update()
        continue
    
    # game setup
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            exit()
        elif not game_active and event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                score = 0
                snake = Snake(arena_grid, CELL_SIZE)
                food = Food(arena_grid, CELL_SIZE, screen)
                game_active = True
                continue
    
    # moving mechanics
    current_time = pygame.time.get_ticks()
    snake.get_input()
    if current_time - last_move_time > snake_move_interval:
        if game_active:
            new_head_position = snake.move()
            last_move_time = current_time
            if snake.eat_food(food.position):
                score += 1
                food.reposition(snake.positions)
            if snake.death_check():
                game_active = False
    
    # rendering engine
    screen.blit(background_surface, (0, 0))
    if game_active:
        food.draw()
        snake.draw(screen, not game_active)
        draw_score(screen, score, font)
    else:
        snake.draw(screen, not game_active)
        draw_game_over(screen, score, font)
    
    pygame.display.update()
    clock.tick(60)
